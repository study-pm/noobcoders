# C# NoobCoder's — Stepik
650a18be6a97eeab17de0580

## Overview

### About this file
The purpose of this file is to provide basic overview, setup, usage and other instructions as well as the general background information about the project.

### Project information

| | |
-- | --
**Title** | C# NoobCoder's
**Url** | https://stepik.org/course/113539/syllabus
**Released** |
**Authors** | NoobCoders
**Publishers** | Stepik
**Catalogues** |
**Discipline** | Software Development
**Subjects** | C#

### Project Structure
This section defines the general project's structure and provides a top-level file/directory layout. Some items may not be present in the actual project or listed here depending on the specific user's configuration and current project state.

```sh
.
├─<📁 .git               # Version control files
├>>📁 .tmp               # Temporary files
├──📁 assets             # Additional value files
├─*📁 img                # Binary graphics
├─*📁 res                # Static resources
├──📁 src                # Source code
├──📁 svg                # Vector graphics
├──📁 wiki               # Knowledge base
├── 🗎 .gitignore          # VCS blob ignores
├── 🗎 notes.md            # Subject information
├── 🗎 README.md           # Project overview
├── 🗎 related.md          # Related sources
├─> 🗎 styles.css          # Custom stylesheet
└─> 🗎 todos.md            # Task list
```

- `─` denotes an item
- `<` denotes a hidden item ingored by default;
- `*` denotes an item present in the project but ignored by VCS (put in the *.gitignore* file);
- `>` denotes an item ignored locally (at specific user's project level — should be put in the *.git/info/exclude* file)
- `>>` denotes an item ignored globally (at specific user's system level — should be put in the *~/.gitignore_global* file)
