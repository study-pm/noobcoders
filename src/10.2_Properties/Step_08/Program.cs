﻿namespace Step_08
{
    internal class Program
    {
        class Date
        {
            public DateTime date { get; set; }
            public Date(DateTime date)
            {
                this.date = date;
            }
            public Date(int day, int month, int year)
            {
                string dateString = month + "/" + day + "/" + year;
                bool isParsed = DateTime.TryParse(dateString, out DateTime parsed);
                if (isParsed)
                {
                    date = parsed;
                    return;
                }
                int y = year;
                int m = month;
                int d = 1;
                if (month > 12)
                {
                    y += month / 12;
                    m = month % 12;
                }
                date = new DateTime(y, m, d);
                date = date.AddDays(day-1);
            }
            private static string Format(DateTime date)
            {
                return $"The {date.Day} of {date.ToString("MMMM yyyy")}";
            }
            public Date Next()
            {
                return new Date(date.AddDays(1));
            }
            public Date Previous()
            {
                return new Date(date.AddDays(-1));
            }
            public void Print()
            {
                Console.WriteLine(Date.Format(date));
            }
            public void PrintBackward(int n)
            {
                for (int i = 1; i < n+1; i++)
                {
                    Console.WriteLine(Date.Format(date.AddDays(-i)));
                }
            }
            public void PrintForward(int n)
            {
                for (int i = 1; i < n+1; i++)
                {
                    Console.WriteLine(Date.Format(date.AddDays(i)));
                }
            }
        }
        static void Main(string[] args)
        {
            string[] input = Console.ReadLine().Split(".");
            Date date = new(int.Parse(input[0]), int.Parse(input[1]), int.Parse(input[2]));
            Console.WriteLine("Текущая дата:");
            date.Print();
            Console.WriteLine("Следующая дата:");
            Date dateNext = date.Next();
            dateNext.Print();
            Console.WriteLine("Предыдущая дата:");
            Date datePrev = date.Previous();
            datePrev.Print();
            Console.WriteLine("Следующие 5 дней:");
            date.PrintForward(5);
            Console.WriteLine("Предыдущие 5 дней:");
            date.PrintBackward(5);
        }
    }
}
