﻿using System.Xml.Linq;

namespace Step_07
{
    public class Vasya
    {
        private string _name;
        private int _age;
        private int _minAge = 0;
        private int _maxAge = 122;
        private string _nameDefault = "Василий";
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < _minAge) _age = _minAge;
                else if (value > _maxAge) _age = _maxAge;
                else _age = value;
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.ToLower() == _nameDefault.ToLower()) _name = _nameDefault;
                else _name = $"Я не {value}, а {_nameDefault}!";
            }
        }
        public Vasya(string name, int age)
        {
            Name = name;
            Age = age;
        }
        public static string GetYearsDeclined(int age)
        {
            string charArr = age.ToString();
            char? charBeforeLast = charArr.Length > 1 ? charArr[charArr.Length - 2] : null;
            char lastChar = charArr[charArr.Length - 1];
            if (charBeforeLast == '1') return "лет";
            return lastChar switch
            {
                '1' => "год",
                '2' or '3' or '4' => "года",
                _ => "лет",
            };
        }
    }
    public class Program
    {
        static public void Main()
        {
            string[] input = Console.ReadLine().Split();

            Vasya user = new(input[0], Convert.ToInt32(input[1]));

            Console.WriteLine(user.Name);
            Console.WriteLine("Мне " + user.Age + " " + Vasya.GetYearsDeclined(user.Age));

        }
    }
}
