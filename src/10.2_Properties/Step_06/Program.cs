﻿namespace Step_06
{
    internal class Program
    {
        class Tree
        {
            public string Name { get; set; }
            private int _height;
            public int Height {
                get
                {
                    return _height;
                }
                set
                {
                    _height = value < 0 ? 1 : value;
                }
            }
            public Tree(string name, int height)
            {
                Name = name;
                Height = height;
                Console.WriteLine($"Вы создали дерево \"{Name}\" высотой {Height} см");
            }
        }
        public static void Main()
        {
            var tree1 = new Tree("сосна", -6);
            var tree2 = new Tree("береза", 80);
        }
    }
}
