﻿namespace Step_09
{
    using System;
    public class MainClass
    {
        public static void Main(string[] args)
        {
            var firstNum = new Num(9);
            var secondNum = new Num(1);
            var thirdNum = new Num(90);
            firstNum.Add(secondNum).AddNum(thirdNum).Print();
        }
    }

    public static class NumExtensions
    {
        public static Num Add(Num left, Num right)
        {
            return new Num(left.Value + right.Value);
        }
    }

    public class Num
    {
        public int Value;
        public Num(int value)
        {
            Value = value;
        }
        public Num Add(Num n)
        {
            return NumExtensions.Add(this, n);
        }
        public Num AddNum(Num n)
        {
            return NumExtensions.Add(this, n);
        }
        public void Print()
        {
            Console.WriteLine(this.Value);
        }
    }
}
