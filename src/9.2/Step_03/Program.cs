﻿using System;

namespace Step_03
{
    public class MainClass
    {
        class Matrix
        {
            dynamic data;
            int rowCount = 0;
            int columnCount = 0;
            public void Read()
            {
                string[] input1 = Console.ReadLine().Split(' ');
                rowCount = int.Parse(input1[0]);
                columnCount = int.Parse(input1[1]);

                data = new double[rowCount, columnCount];

                for (int i = 0; i < rowCount; i++)
                {
                    string[] input = Console.ReadLine().Split(' ');
                    for (int j = 0; j < input.Length; j++)
                    {
                        data[i, j] = double.Parse(input[j]);
                    }
                }
            }
            public void Multiply(double n)
            {
                for (int i = 0; i < rowCount; i++)
                {
                    for (int j = 0; j < columnCount; j++)
                    {
                        data[i, j] *= n;
                    }
                }
            }
            public void Write()
            {
                for (int i = 0; i < rowCount; i++)
                {
                    Console.Write(data[i, 0]);
                    for (int j = 1; j < columnCount; j++)
                    {
                        Console.Write(" " + data[i, j]);
                    }
                    Console.WriteLine();
                }
            }
        }
        public static void Main()
        {
            var A = new Matrix();
            A.Read();
            double n = double.Parse(Console.ReadLine());
            A.Multiply(n);
            A.Write();
        }
    }
}
