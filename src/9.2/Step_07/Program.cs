﻿using System;

namespace Step_07
{
    public class MainClass
    {
        class Matrix
        {
            dynamic data;
            int rowCount = 0;
            int columnCount = 0;
            public Matrix() { }
            public Matrix(int rowCount, int columnCount)
            {
                data = new double[rowCount, columnCount];
                this.rowCount = rowCount;
                this.columnCount = columnCount;
            }
            public void Read()
            {
                string[] input1 = Console.ReadLine().Split(' ');
                rowCount = int.Parse(input1[0]);
                columnCount = int.Parse(input1[1]);

                data = new double[rowCount, columnCount];

                for (int i = 0; i < rowCount; i++)
                {
                    string[] input = Console.ReadLine().Split(' ');
                    for (int j = 0; j < input.Length; j++)
                    {
                        data[i, j] = double.Parse(input[j]);
                    }
                }
            }
            public void Multiply(double n)
            {
                for (int i = 0; i < rowCount; i++)
                {
                    for (int j = 0; j < columnCount; j++)
                    {
                        data[i, j] *= n;
                    }
                }
            }
            static public Matrix Add(Matrix a, double n)
            {
                Matrix sum = new(a.rowCount, a.columnCount);
                for (int i = 0; i < a.rowCount; i++)
                {
                    for (int j = 0; j < a.columnCount; j++)
                    {
                        sum.data[i, j] = a.data[i, j] + n;
                    }
                }
                return sum;
            }
            static public Matrix Add(Matrix a, Matrix b)
            {
                Matrix sum = new(a.rowCount, a.columnCount);
                for (int i = 0; i < a.rowCount; i++)
                {
                    for (int j = 0; j < a.columnCount; j++)
                    {
                        sum.data[i, j] = a.data[i, j] + b.data[i, j];
                    }
                }
                return sum;
            }
            static public Matrix Multiply(Matrix a, double n)
            {
                Matrix product = new(a.rowCount, a.columnCount);
                for (int i = 0; i < a.rowCount; i++)
                {
                    for (int j = 0; j < a.columnCount; j++)
                    {
                        product.data[i, j] = a.data[i, j] * n;
                    }
                }
                return product;
            }
            static public Matrix Multiply(Matrix a, Matrix b)
            {
                Matrix product = new(a.rowCount, b.columnCount);
                for (int i = 0; i < a.rowCount; i++)
                {
                    for (int j = 0; j < b.columnCount; j++)
                    {
                        for (int x = 0; x < a.columnCount; x++)
                        {
                            product.data[i, j] += a.data[i, x] * b.data[x, j];
                        }
                    }
                }
                return product;
            }
            static public Matrix Subtract(Matrix a, double n)
            {
                return Matrix.Add(a, -1 * n);
            }
            static public Matrix Subtract(Matrix a, Matrix b)
            {
                Matrix subtrahand = Matrix.Multiply(b, -1);
                return Matrix.Add(a, subtrahand);
            }
            static public Matrix Transpose(Matrix src)
            {
                Matrix transposed = new(src.columnCount, src.rowCount);
                for (int i = 0; i < src.rowCount; i++)
                {
                    for (int j = 0; j < src.columnCount; j++)
                    {
                        transposed.data[j, i] = src.data[i, j];
                    }
                }
                return transposed;
            }
            public void Sum(Matrix summand)
            {
                for (int i = 0; i < rowCount; i++)
                {
                    for (int j = 0; j < columnCount; j++)
                    {
                        data[i, j] += summand.data[i, j];
                    }
                }
            }
            public void Write()
            {
                for (int i = 0; i < rowCount; i++)
                {
                    Console.Write(data[i, 0]);
                    for (int j = 1; j < columnCount; j++)
                    {
                        Console.Write(" " + data[i, j]);
                    }
                    Console.WriteLine();
                }
            }
        }
        public static void Main()
        {
            var A = new Matrix();
            A.Read();

            Matrix C = Matrix.Transpose(A);
            C.Write();
        }
    }
}
