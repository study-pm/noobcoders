﻿/* Реализуйте класс `MyString`, который будет иметь поле строки и следующие методы:
 * - метод Reverse(), который возвращает строку в перевернутом виде;
 * - метод `UcFirst(), который возвращает строку, сделав ее первую букву заглавной;
 * - метод `UcWords(), который возвращает строку, сделав заглавной первую букву каждого слова этой строки
 */

namespace Step_08
{
    public class MainClass
    {
        public class MyString
        {
            public string value;
            public string Reverse()
            {
                if (value.Length == 0) return "";
                char[] charArray = value.ToCharArray();
                Array.Reverse(charArray);
                return new string(charArray);
            }
            public string UcFirst()
            {
                if (value.Length == 0) return "";
                return Char.ToUpper(value[0]) + value.Substring(1);
            }
            public string UcWords()
            {
                if (value.Length == 0) return "";
                string[] words = value.Trim().Split(" ");
                for (int i = 0; i < words.Length; i++)
                {
                    if (words[i].Length == 0) words[i] = "";
                    else words[i] = Char.ToUpper(words[i][0]) + words[i][1..];
                }
                return String.Join(" ", words);
            }
        }
        public static void Main()
        {
            var myS = new MyString();

            // Input
            myS.value = Console.ReadLine();

            // Output
            Console.WriteLine(myS.Reverse());
            Console.WriteLine(myS.UcFirst());
            Console.WriteLine(myS.UcWords());
        }
    }
}

// Sample input 1: hello world!
// Sample input 2: hello 
// Sample input 3: hello world p
// Sample input 4:

/* Sample output 1:
    !dlrow olleh
    Hello world!
    Hello World!
*/
/* Sample output 2:
     olleh
    Hello
    Hello
 */
/* Sample output 3:
    p dlrow olleh
    Hello world p
    Hello World P
 */
/* Sample output 4:
 *
 *
 *
 */
