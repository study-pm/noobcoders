﻿/* В пустой комнате на полу находится уснувшая муха с координатами (X, Y, Z).
 * Паук, находившийся на одной из стен, на полу, на потолке или подвешенный на паутине, начал двигаться к ней по кратчайшему пути.
 *
 * Входные данные:
 * - координаты мухи (X, Y, Z);
 * - координаты паука (X, Y, Z);
 *
 * Создайте класс точки с атрибутами координат и методы, вычисляющие:
 * 1. Расстояние между мухой и пауком по прямой линии
 * 2. Путь, который необходимо пройти пауку до мухи.  Паук может ***спускаться*** только вертикально вниз.
 *
 * Ответы должны быть даны с округлением до 5 знаков после запятой
*/

using System;

public class MainClass
{
    public class Position
    {
        public int x;
        public int y;
        public int z;
        public Position(string[] coordinates)
        {
            x = Convert.ToInt32(coordinates[0]);
            y = Convert.ToInt32(coordinates[1]);
            z = Convert.ToInt32(coordinates[2]);
        }
    }
    public static dynamic GetDistance(Position spider, Position fly)
    {
        return Math.Sqrt(Math.Pow((spider.x - fly.x), 2) + Math.Pow((spider.y - fly.y), 2) + Math.Pow((spider.z - fly.z), 2));
    }
    public static double GetTravel(Position spider, Position fly)
    {
        return (spider.z - fly.z) + Math.Sqrt(Math.Pow((spider.x - fly.x), 2) + Math.Pow((spider.y - fly.y), 2));
    }
    public static void Main()
    {
        //используйте эти массивы строк для инициализации объектов мухи и паука
        var s1 = Console.ReadLine().Split(' ');
        var s2 = Console.ReadLine().Split(' ');

        Position fly = new Position(s1);
        Position spider = new Position(s2);

        if (fly.z != 0)
        {
            Console.WriteLine("Муха должна быть на полу!");
            return;
        }

        var distance = GetDistance(spider, fly);
        var travel = GetTravel(spider, fly);

        //Вывод ответа
        Console.WriteLine("Расстояние: " + ((fly.x == spider.x && fly.y == spider.y) ? distance : distance.ToString("N5")));
        Console.WriteLine("Путь: " + ((fly.x == spider.x && spider.y % fly.y == 0) ? travel : travel.ToString("N5")));
    }
}
