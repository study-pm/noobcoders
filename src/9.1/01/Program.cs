﻿namespace Task_01
{
    public class Worker
    {
        public string name;
        public string surname;
        public int rate;
        public int days;

        public void GetFullName()
        {
            Console.WriteLine($"Фамилия сотрудника: {surname}");
            Console.WriteLine("Имя сотрудника: " + name);
        }
        public int GetSalary()
        {
            return rate * days;
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");

            Worker worker = new Worker();
            worker.surname = "Иванов";
            worker.name = "Иван";
            worker.rate = 120;
            worker.days = 30;
            Console.WriteLine(worker.GetSalary());
            worker.GetFullName();
        }
    }
}
