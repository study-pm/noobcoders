﻿namespace Step_07
{
    internal class Program
    {
        public class Barrel
        {
            public string material;
            public int volume;
            public void ToChange(int volume)
            {
                this.volume = volume;
            }
            public void ToChange(string material)
            {
                this.material = material;
            }
        }
        public static void Main()
        {
            var barrel = new Barrel();
            barrel.material = "oak";
            barrel.volume = 200;
            barrel.ToChange(100);
            barrel.ToChange("beech");
            Console.WriteLine($"{barrel.material}, {barrel.volume}");
        }
    }
}
