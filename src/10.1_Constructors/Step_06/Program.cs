﻿namespace Step_06
{
    public class Program
    {
        public class Watch
        {
            public string Name { get; set; }
            public string Type { get; set; }
            public decimal Value { get; set; }
            public Watch(string name, string type, decimal value)
            {
                Name = name;
                Type = type;
                Value = value;
            }
            public void Print()
            {
                Console.WriteLine($"Часы {Name}, тип: {Type}, цена: {Value.ToString("N2")}$");
            }
        }
        public static void Main()
        {
            var myWatch = new Watch("Casio", "electronic", 15.99M);
            myWatch.Print();
        }
    }
}
