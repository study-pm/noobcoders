﻿namespace Step_08
{
    internal class Program
    {
        public class Sequence
        {
            private string _value = "";
            public string Value {
                get { return _value; }
                set { _value = value; }
            }
            public Sequence(int trailing)
            {
                for (int i = 0; i < trailing+1; i++) Value += i + " ";
                Print();
            }
            public Sequence(int leading, int trailing)
            {
                for (int i = leading; i < trailing+1; i++) Value += i + " ";
                Print();
            }
            public Sequence(char trailing)
            {
                // Convert to int and back
                // for (int i = 97; i < (int)trailing+1; i++) Value += (char)i + " ";

                // Also works with chars
                for (char i = 'a'; i < trailing+1; i++) Value += i + " ";
                Print();
            }
            public Sequence(char leading, char trailing)
            {
                // Convert to int and back
                // for (int i = (int)leading; i < (int)trailing+1; i++) Value += (char)i + " ";

                // Also works with chars
                for (char i = leading; i < trailing + 1; i++) Value += i + " ";
                Print();
            }
            public void Print()
            {
                Value.Trim();
                Console.WriteLine(Value);
            }
        }
        static void Main(string[] args)
        {
            string[] input_int = Console.ReadLine().Split(" ");
            Sequence seq_int;
            if (input_int.Length == 1) seq_int = new(int.Parse(input_int[0]));
            else seq_int = new(int.Parse(input_int[0]), int.Parse(input_int[1]));

            string[] input_str = Console.ReadLine().Split(" ");
            Sequence seq_char;
            if (input_str.Length == 1) seq_char = new(input_str[0][0]);
            else seq_char = new(input_str[0][0], input_str[1][0]);
        }
    }
}
