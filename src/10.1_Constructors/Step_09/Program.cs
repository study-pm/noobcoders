﻿using System.Text;

namespace Step_09
{
    internal class Program
    {
        class Money
        {
            public decimal Amount { get; set; }
            public Money(decimal value)
            {
                Amount = value;
            }
            public Money(string value, string unit)
            {
                Amount = HandleInput(value, unit);
            }
            public Money(string rVal, string rUnit, string cVal, string cUnit)
            {
                if (rUnit == "коп." && cUnit == "р.")
                {
                    Console.WriteLine("Рубли и копейки перепутаны местами!");
                    return;
                    // throw new ArgumentException("Рубли и копейки перепутаны местами!");
                }
                Amount = HandleInput(rVal, rUnit);
                Amount += HandleInput(cVal, cUnit);
            }
            private decimal HandleInput(string val, string unit)
            {
                int value = int.Parse(val);
                HandleNegativeValue(value);
                switch (unit)
                {
                    case "р.":
                        return value;
                    case "коп.":
                        return (decimal)value / 100;
                    default:
                        throw new ArgumentException($"Invalid unit: {unit}");
                }
            }
            private void HandleNegativeValue(int value)
            {
                if (value < 0)
                {
                    Console.WriteLine("Не может быть отрицательным!");
                    return;
                    // throw new ArgumentException("Не может быть отрицательным!");
                }
            }
            public int GetCopecks() => (int)((Amount % 1) * 100);
            public int GetRoubles() => (int)(Amount - Amount % 1);
            public void Print()
            {
                int r = GetRoubles();
                int c = GetCopecks();
                Console.WriteLine((r != 0 ? (r + " р. ") : "") + c + " коп.");
            }
            public void PrintTransferCost(double comissionRate)
            {
                Money comission = new(Math.Round(Amount * (decimal)comissionRate, 2));
                Money total = Money.Sum(this, comission);
                total.Print();
            }
            public static Money Difference(Money a, Money b)
            {
                decimal diff = a.Amount - b.Amount;
                return new Money(diff < 0 ? diff * (-1) : diff);
            }
            public static Money Sum(Money a, Money b)
            {
                return new Money(a.Amount + b.Amount);
            }

        }
        static void Main(string[] args)
        {
            Console.Write("Enter amount: ");
            string[] input = Console.ReadLine().Split(' ');

            Money amt;
            if (input.Length == 2) amt = new(input[0], input[1]);
            else amt = new(input[0], input[1], input[2], input[3]);

            amt.Print();

            Money sum = Money.Sum(amt, amt);
            sum.Print();

            Money diff = Money.Difference(amt, amt);
            diff.Print();

            amt.PrintTransferCost(0.05);
        }
    }
}
